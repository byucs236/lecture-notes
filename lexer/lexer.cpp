#include <string>
#include <iostream>
#include <vector>
#include <ctype.h>

using namespace std; 

enum Token {COMMA, PERIOD, Q_MARK, LEFT_PAREN, RIGHT_PAREN, MULTIPLY, ADD, SCHEMES, FACTS, RULES, QUERIES, ID, STRING, COMMENT, WHITESPACE, UNDEFINED, MEOF};

string toString(Token t) {
        switch(t) {
        case COMMA: return ("COMMA");
        case PERIOD: return ("PERIOD");
        case Q_MARK: return ("Q_MARK");
        case LEFT_PAREN: return ("LEFT_PAREN");
        case RIGHT_PAREN: return ("RIGHT_PAREN");
        case MULTIPLY: return ("MULTIPLY");
        case ADD: return ("ADD");
        case SCHEMES: return ("SCHEMES");
        case FACTS: return ("FACTS");
        case RULES: return ("RULES");
        case QUERIES: return ("QUERIES");
        case ID: return ("ID");
        case STRING: return ("STRING");
        case COMMENT: return ("COMMENT");
        case WHITESPACE: return ("WHITESPACE");
        case UNDEFINED: return ("UNDEFINED");
        case MEOF: return ("MEOF");
        default: break;
        }
        return ("Uh oh!");
}


class Automaton {
public:
    virtual ~Automaton() {}
    virtual int read(const string& input) const = 0;
    virtual Token makeToken(const string& s) const = 0;
};

class StringMatch : public Automaton {
    string stringToMatch;
    Token tokenToMake;

public:

    StringMatch(const string& s, Token t) : 
        stringToMatch(s), tokenToMake(t) {
    }

    int read(const string& input) const {
        if (input.size() < stringToMatch.size()) {
            return 0;
        }
        
        int i = 0;
        for (const auto& c : stringToMatch) {
            if (c != input[i]) {
                return 0;
            }
            ++i;
        }
        return stringToMatch.size();
    }

    Token makeToken(const string& s) const {
        cout << toString(tokenToMake) << "(" << s << ")" << endl;
        return tokenToMake;
    }
};

class WhiteSpace : public Automaton {
public:

    int read(const string& input) const {
        int i = 0;
        while(isspace(input[i])) {
            ++i;
        }
        return i;
    }

    Token makeToken(const string& s) const {
        cout << toString(WHITESPACE) << endl;
        return WHITESPACE;
    }

};

class Identifier : public Automaton {

    int s0(int i, const string& input) const {
        const auto& c = input[i];
        if (i < input.size() && isalpha(c)) {
            return s1(++i, input);
        }

        return 0;
    }

    int s1(int i, const string& input) const {
        const auto& c = input[i];
        if (i < input.size() && (isalpha(c) || isdigit(c))) {
            return s1(++i, input);
        }
        
        return i;
    }


public:

    int read(const string& input) const {
        return s0(0, input);
    }

    Token makeToken(const string& s) const {
        cout << toString(ID) << "(" << s << ")" << endl;
        return ID;
    }
};

void generateTokens(const vector<Automaton*> machines, string input) {
    while (input.size() > 0) {
        
        int max = 0;
        Automaton *maxAutomaton;
        for (const auto& m : machines) {
            int i = m->read(input);
            if (i > max) {
                max = i;
                maxAutomaton = m;
            }
        }

        if (max > 0) {
            maxAutomaton->makeToken(input.substr(0,max));
            input.erase(0,max);
        } else {
            cout << "BAD(" << input.front() << ")" << endl;
            input.erase(input.begin());
        }

    }
}


int main(int argc, char* argv[]) {
    string input(argv[1]);
    cout << "Input:" << endl << input << endl;
    vector<Automaton*> automata;
    automata.push_back(new StringMatch(",", COMMA));
    automata.push_back(new StringMatch(".", PERIOD));
    automata.push_back(new StringMatch("?", Q_MARK));
    automata.push_back(new StringMatch("Schemes", SCHEMES));
    automata.push_back(new StringMatch("Facts", FACTS));
    automata.push_back(new Identifier());
    automata.push_back(new WhiteSpace());
    generateTokens(automata, input);

    for (auto a : automata) {
        delete a;
    }
    
    return 0;
}
