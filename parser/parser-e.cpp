#include <queue>
#include <assert.h>
#include <iostream>
#include <string>

using namespace std;

enum token {PLUS, ASTERISK, LEFT_PAREN, RIGHT_PAREN, ID, MEOF};

void e(queue<token>& tokens);
void e_prime(queue<token>& tokens);
void t(queue<token>& tokens);
void t_prime(queue<token>& tokens);
void f(queue<token>& tokens);

string to_string(token t) {
	switch(t) {
	case PLUS: return ("+");
	case ASTERISK: return ("*");
	case LEFT_PAREN: return ("(");
	case RIGHT_PAREN: return (")");
	case ID: return ("ID");
	case MEOF: return ("MEOF");
	default: break;
	}
	return ("Uh oh!");
}

void initialize_tokens(queue<token>& tokens) {
	tokens.push(LEFT_PAREN);
	tokens.push(ID);
	tokens.push(PLUS);
	tokens.push(ID);
	tokens.push(RIGHT_PAREN);
	tokens.push(ASTERISK);
	tokens.push(ID);
	tokens.push(MEOF);
}

void check(queue<token>& tokens, token expected) {
	token given = tokens.front();
	tokens.pop();

	if (given != expected) {
		throw(to_string(given));
	}

	cout << to_string(given) << " ";
}

int main(int argc, char* argv[]) {
	queue<token> tokens;
	initialize_tokens(tokens);
	try {
		check(tokens, ASTERISK);
	}
	catch (std::string s) {
		cerr << "Failure!" << endl << "  " << s << endl;
	}
}
