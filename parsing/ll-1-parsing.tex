\documentclass[10pt,conference]{IEEEtran}
% \usepackage{booktabs, amsmath, amssymb, stmaryrd, paralist, caption, comment, algorithm, algpseudocode, mathpartir, listings, graphicx, epstopdf, multicol, tikz-cd, xifthen, float}
\usepackage{amsmath, amsfonts}

\newcommand{\todo}[1]{\textbf{TODO: #1}}

\input{macros}
\newcommand{\emptystr}{\epsilon}
\newcommand{\eof}{\syn{\$}}
\newtheorem{definition}{Definition}
\newcommand{\Vocab}{\mathbb{V}}
\newcommand{\StartSymb}{\mathbb{S}}
\newcommand{\Terminals}{\mathbb{T}}
\newcommand{\Productions}{\mathbb{P}}
\newcommand{\reducesto}{\stackrel{\syn{*}}{\produces}}
\newcommand{\FIRST}{\ensuremath{\var{FIRST}}}
\newcommand{\FOLLOW}{\ensuremath{\var{FOLLOW}}}

\begin{document}
\title{LL(1) Parsing}

\author{Peter Aldous}
\maketitle

% \begin{abstract}

% \end{abstract}

\section{Introduction}
\label{sec:intro}

Our textbook gives only a cursory explanation of parsing.
While there are texts that treat parsing in detail, there is no need
to purchase an additional textbook to use a small fraction of its
material (especially when the information we use is commonly
available).
Since we have no readily available reading material on the topic of
LL(1) parsing, I have written this short document in the hopes that it
would be helpful to students who need to learn parsing early in their
computer science education (e.g., students who take a discrete math
course that treats parsing in their third semester).

In this document, I will present mathematical definitions and give
prose descriptions of those definitions.
If you discover a difference between the definitions and the
descriptions \emph{trust the definitions}.

This document assumes that the reader is familiar with lexical
analysis and with the structure of phase-structure grammars, as well
as a small amount of programming.

\section{Motivation}
\label{sec:motivation}

It is necessary to understand a program's structure in order to
execute or analyze it.
Even after a lexical analysis has been performed, the token stream is
simply a sequence.
For example, the \syn{if} keyword in Java is related to a condition
and to subsequent statements.
These statements may themselves be or contain \syn{if} statements.
In this case, the relationship between each \syn{if} statement and its
condition and branch or branches may not be immediately obvious.
Parsing provides the structure that clarifies these relationships.

This document concerns \emph{automated} LL(1) parsing.
The first L in LL(1) specifies a left-to-right scan of the tokens;
that is, tokens are read in the order they are produced, from the
beginning of the document to the end.
The second L in LL(1) states that the derivation produced is a
leftmost derivation: only the leftmost nonterminal is reduced.
The 1 in LL(1) states that only one token of lookahead is required; in
other words, it is always possible to determine the next action by
looking at the next token in the stream.
In practice, most programming languages use LL(2) parsing.
LL(2) parsing, however, adds complications that are not necessary for
an early discrete math class.

The grammar in \figref{fig:grammar} will serve as a running example
throughout the document.
\eof\ will mark the end of input.
$\emptystring$ represents the empty string.
The symbols $\Vocab$, $\StartSymb$, $\Terminals$, and $\Productions$
are the vocabulary, start symbol, set of terminals, and set of
productions in a phase-structure grammar.
$\alpha$ and $\beta$ are used throughout this document as arbitrary
strings comprising terminals and/or nonterminals; in other words,
$\alpha, \beta \in \Vocab\syn{*}$.

\begin{figure}
\begin{align*}
E &\produces E + T \mid T \\
T &\produces T * F \mid F \\
F &\produces \syn{(} E \syn{)} \mid \syn{int}
\end{align*}
\caption{An arithmetic grammar}
\label{fig:grammar}
\end{figure}

The goal of all of the formalisms introduced in this document is to
allow for the creation of a \defterm{parse table}.
A parse table contains a grammar's nonterminals on the left and its
terminals (as well as the end-of-input pseudoterminal \eof) along the
top.
An LL(1) parse is performed by identifying the leftmost nonterminal
and the next token from input and looking up the appropriate
production in the parse table.
This process is presented in detail in \secref{sec:use-parse-table}.

\section{Restructuring the grammar}
\label{sec:restructuring}

Many grammars are amenable to LL(1) parsing, but only after they have
been restructured.
This section demonstrates the most common methods of restructuring
grammars to this end.

\subsection{Removal of left recursion}
\label{sec:remove-left-recursion}

When $E$ reduces to $E + T$, it produces a string that begins with
$E$.
As a result, it is possible for a parse to loop indefinitely instead
of terminating, even on a finite input.
The same thing occurs when $T$ reduces to $T * F$.

In the more general case, a left-recursive production takes the form
$N \produces N\alpha \mid \beta$, where $\alpha, \beta \in \Vocab\syn{*}$
(that is, $\alpha$ and $\beta$ are arbitrary strings comprising
terminals and/or nonterminals).
$\beta$ necessarily starts with something besides $N$; otherwise, it
would be impossible for $N$ to reduce to a string in any language (as
these strings must comprise only terminals).
In this grammar, the first production produces a copy of $\alpha$ to
the right of $N$. This rule may be applied any number of times,
resulting in strings of the form $N\alpha\syn{*}$.
The second production $N \produces \beta$ must eventually be used to
reduce the string in a way that removes $N$. When this occurs, the
string produced is of the form $\beta\alpha\syn{*}$.

These productions may be rewritten to produce the same language with
right recursion as follows:

\begin{align*}
N &\produces \beta N' \\
N' &\produces \alpha N' \mid \emptystring\ .
\end{align*}

Even more generally, there may be multiple productions of either form.
In this more general case, the resulting string takes the form
$\beta_{i}\alpha_{i}\syn{*}$, where the choice of $i$ may change for
each instance of some $\alpha_{i}$.
\figref{fig:remove-left-recursion} shows the transformation in this
case.

\begin{figure}
\begin{align*}
N &\produces N\alpha_{1} \mid N\alpha_{2} \mid \ldots \mid N\alpha_{n}
\mid \beta_{1} \mid \beta_{2} \mid \ldots \mid \beta_{m} \\
\text{becomes} \\
N &\produces \beta_{1}N' \mid \beta_{2}N' \mid \ldots \mid \beta_{m}N'
\\
N' &\produces \alpha_{1}N' \mid \alpha_{2}N' \mid \ldots \mid
\alpha_{n}N' \mid \emptystring
\end{align*}
\caption{Removal of left recursion}
\label{fig:remove-left-recursion}
\end{figure}

Restructuring the grammar in \figref{fig:grammar} yields the grammar
in \figref{fig:ll-grammar}.

\begin{figure}
\begin{align*}
E &\produces T E' \\
E' &\produces + T E' \mid \emptystring \\
T &\produces F T' \\
T' &\produces * F T' \mid \emptystring \\
F &\produces \syn{(} E \syn{)} \mid \syn{int}
\end{align*}
\caption{The arithmetic grammar without left recursion}
\label{fig:ll-grammar}
\end{figure}

\subsection{Left factoring}
\label{sec:left-factoring}

In some grammars, different productions from the same nonterminal may
begin with a common prefix or factor.
In order to ensure that only one token of lookahead is required, these
grammars must be left factored to produce an LL(1) grammar.

Consider the grammar in \figref{fig:rr-grammar}, which contains a
right recursive variant of the example grammar.
Notice that this structure enforces right associativity on addition
and multiplication.
From a mathematical perspective, addition and multiplication are
associative, so this makes no difference as far as correctness.
However, this is contrary to the C specification.
More importantly, this grammar would result in incorrect substitution
and division, as these operations are \emph{not} associative.

\begin{figure}
\begin{align*}
E &\produces T + E \mid T \\
T &\produces F * T \mid F \\
F &\produces \syn{(} E \syn{)} \mid \syn{int}
\end{align*}
\caption{A right recursive arithmetic grammar}
\label{fig:rr-grammar}
\end{figure}

Although this grammar is not left recursive (and therefore is amenable
to LL parsing), it is not clear which production should be used when
examining a single token of input.
For example, $E$ may reduce to $T + E$ or to $T$.
In either case, the next token could be $\syn{(}$ or an integer.

This transformation relies on the observation that the grammar may be
rewritten in a way that parses the $T$ common to both productions and
then decides what to do with the remainder of the input.

More generally, a nonterminal $N$ may reduce to any number of strings,
which can be partitioned into those that start with some common factor
$M$ and those that do not.
The productions that begin with $M$ may be condensed into a single
production $N \produces M X$, where $X$ is some new nonterminal.
$X$ can produce the various suffixes that appear after $M$ in the
original grammar:

\begin{align*}
N &\produces M \alpha_{1} \mid M \alpha_{2} \mid \ldots \mid M
\alpha_{n} \mid \beta_{1} \mid \beta_{2} \mid \ldots \mid \beta_{m} \\
\text{becomes} \\
N &\produces M X \mid \beta_{1} \mid \beta_{2} \mid \ldots \mid \beta_{m} \\
X &\produces \alpha_{1} \mid \alpha_{2} \mid \ldots \mid \alpha_{n}\ .
\end{align*}

Applying this process to the grammar in \figref{fig:rr-grammar}
produces the grammar in \figref{fig:left-factored}.

\begin{figure}
\begin{align*}
E &\produces T X \\
X &\produces + E \mid \emptystring \\
T &\produces F Y \\
Y &\produces * T \mid \emptystring \\
F &\produces \syn{(} E \syn{)} \mid \syn{int}
\end{align*}
\caption{The grammar in \figref{fig:rr-grammar} left factored}
\label{fig:left-factored}
\end{figure}

\section{\FIRST\ and \FOLLOW\ sets}
\label{sec:first-follow}

The first step towards creating a parse table is to compute the \FIRST
and \FOLLOW\ sets for each nonterminal in the grammar.
Strictly speaking, the \FOLLOW\ sets are only required for
nonterminals whose \FIRST\ sets contain $\emptystring$; however, the
\FOLLOW\ set for one nonterminal is frequently useful for the
computation of other \FOLLOW\ sets.

\subsection{\FIRST\ sets}
\label{sec:first}

The \FIRST\ set of a nonterminal $N$ is the set of terminals that may
appear in some string derived from $N$.
It includes $\emptystring$ whenever the empty string may be derived
from $N$.
Intuitively, it is the set of all tokens that may appear when parsing
$N$; a parser that encounters a token not in the \FIRST\ set of its
leftmost nonterminal will never parse successfully and can immediately
report an error.
The definition of a \FIRST\ set is given in \defref{def:first}.

\begin{definition}
\begin{align*}
\FIRST(N) &= \left\{t \in \Terminals \mid N \reducesto t\alpha\right\}
\union \var{ML}(N) \\
\var{ML}(N) &= \begin{cases}
\left\{\emptystring\right\} &N \reducesto \emptystring \\
\emptyset &\text{otherwise}\ .
\end{cases}
\end{align*}
\label{def:first}
\end{definition}

\FIRST\ sets are usually easiest to compute from the bottom of a
grammar to the top, as the bottommost productions usually produce more
terminals.
Nonterminals defined towards the top tend to reference \FIRST\ sets
for nonterminals defined below.

The grammar in \figref{fig:ll-grammar} has five nonterminals.
The bottommost terminal is $F$, which has two productions.
As this is a context-free grammar, terminals produced can never be
removed.
Accordingly, every string derived from $F$ must either begin with
\syn{(} or \syn{int}.
Therefore, $\FIRST(F) = \left\{\syn{(}, \syn{int}\right\}$.

Since the first production for $T'$ begins with $*$, $* \in
\FIRST(T')$.
Since $T'$ can reduce to the empty string, $\emptystring$ is also in
$\FIRST(T')$.
$\FIRST(T') = \left\{*, \emptystring\right\}$.

The computation of $\FIRST(T)$ makes use of the already-computed
$\FIRST(F)$.
Everything reduced from $T$ begins with $F$, so $\FIRST(T)$ includes
all of the terminals in $\FIRST(F)$.
Since $F$ cannot reduce to the empty string, neither can $T$.
Accordingly, $\FIRST(T) = \left\{\syn{(}, \syn{int}\right\}$.

The other \FIRST\ sets are computed in the same way, resulting in the
following values:

\begin{align*}
\FIRST(E) &= \left\{\syn{(}, \syn{int}\right\} \\
\FIRST(E') &= \left\{+, \emptystring\right\} \\
\FIRST(T) &= \left\{\syn{(}, \syn{int}\right\} \\
\FIRST(T') &= \left\{*, \emptystring\right\} \\
\FIRST(F) &= \left\{\syn{(}, \syn{int}\right\}\ .
\end{align*}

\subsection{\FOLLOW\ sets}
\label{sec:follow}

The \FOLLOW\ set of a nonterminal $N$ is the set of terminals that may
appear after $N$ in some string derived from the start symbol.
For convenience, \eof\ is treated as a terminal for this purpose and
may appear in a \FOLLOW\ set.
(In a language whose lexical specification produces an explicit \eof
token, this special case is unnecessary.)
\FOLLOW\ sets are formally defined in \defref{def:follow}.

\begin{definition}
\begin{align*}
\FOLLOW(N) &= \left\{t \in \Terminals \mid \StartSymb \reducesto
\alpha N t \beta\right\}
\union \var{MB}(N) \\
\var{MB}(N) &= \begin{cases}
\left\{\eof\right\} &\StartSymb \reducesto \alpha N \\
\emptyset &\text{otherwise}\ .
\end{cases}
\end{align*}
\label{def:follow}
\end{definition}

\FOLLOW\ sets are usually easiest to compute from the top of the
grammar down.
In the case of the grammar in \figref{fig:ll-grammar}, the \FOLLOW\
set of $E$ necessarily includes \eof, as $E$ (the start symbol in this
grammar) reduces after zero steps to a string ending in $E$.
$E$ also appears in parenthetical statements, so \syn{)} may also
appear after $E$.
Because $E$ can appear in no other circumstances (i.e., it appears
nowhere else in the grammar), nothing else may follow it and
$\FOLLOW(E) = \left\{), \eof\right\}$.

$\FOLLOW(E')$ makes use of $\FOLLOW(E)$, as $E$ reduces to a string
ending in $E'$.
As a result, any terminal that can occur after $E$ may also appear
after $E'$: $\alpha E t \beta \produces \alpha T E' t \beta$.
The production $E' \produces + T E'$ can never change what follows
$E'$; $\alpha E' t \beta \produces \alpha + T E' t \beta$.
Lastly, the production $E' \produces \emptystring$ removes $E'$,
producing a string where no terminal follows that particular instance
of $E'$.
As a result, $\FOLLOW(E') = \FOLLOW(E) = \left\{\syn{)},
\eof\right\}$.

$T$ can only ever be produced with $E'$ following it.
Accordingly, any terminal that can appear at the beginning of a string
derived from $E'$ may appear after $T$: $\left(\FIRST(E') -
\left\{\emptystring\right\}\right) \subseteq \FOLLOW(T)$.
Additionally, $E'$ can reduce to the empty string (since $\emptystring
\in \FIRST(E')$).
As a result, $\FOLLOW(E') \subseteq \FOLLOW(T)$.
The result is that $\FOLLOW(T) = \left(\FIRST(E') -
\left\{\emptystring\right\}\right) \union \FOLLOW(E') =
\left\{+, \syn{)}, \eof\right\}$.

$T$ and $T'$ have a similar relationship to that of $E$ and $E'$.
Once their \FOLLOW\ sets are computed, the $\FOLLOW(F)$ is
straightforward.
The resulting \FOLLOW\ sets follow:

\begin{align*}
\FOLLOW(E) &= \left\{\syn{)}, \eof\right\} \\
\FOLLOW(E') &= \left\{\syn{)}, \eof\right\} \\
\FOLLOW(T) &= \left\{+, \syn{)}, \eof\right\} \\
\FOLLOW(T') &= \left\{+, \syn{)}, \eof\right\} \\
\FOLLOW(F) &= \left\{+, *, \syn{)}, \eof\right\}\ .
\end{align*}

\section{Creating a parse table}
\label{sec:create-parse-table}

With the \FIRST\ and \FOLLOW\ sets defined for a grammar's
nonterminals, the grammar's LL(1) parse table can be created.
The parse table contains the grammar's nonterminals at the head of the
rows and the grammar's terminals (including \eof\ but \emph{not}
including $\emptystring$) at the head of the columns.
Each cell in the table is either empty or contains the right-hand side
of some production in the grammar whose left-hand side is the
nonterminal at the head of its row.
Nothing besides the right-hand side of a production whose left-hand
side is at the head of the row may ever be placed in the table.

To populate the row in the parse table with $N$ at its head, examine
all of the productions in $\Productions$ with $N$ on the left-hand
side.
For each terminal $t \in \FIRST(N)$, choose the production that
reduces $N$ in that direction.
(For every grammar at this level, this choice should be trivial.)
Place the right-hand side of this production in the cell belonging to
row $N$ and column $t$.
Whenever $\emptystring$ appears in $\FIRST(N)$, place the right-hand
side of the production that moves in the direction of $\emptystring$
(in this grammar, this always happens directly: $N \produces
\emptystring$) in the cell for each terminal in $\FOLLOW(N)$.

The LL(1) parse table for the grammar in \figref{fig:ll-grammar} is
\tabref{tab:parse-table}.

\begin{table}
\begin{center}
\begin{tabular}{|c || c | c | c | c | c | c |}
\hline
 & + & * & \syn{int} & \syn{(} & \syn{)} & \eof \\
\hline
\hline
E & & & T E' & T E' & & \\
\hline
E' & + T E' & & & & \emptystring & \emptystring \\
\hline
T & & & F T' & F T' & & \\
\hline
T' & \emptystring & * F T' & & & \emptystring & \emptystring \\
\hline
F & & & \syn{int} & \syn{(} E \syn{)} & & \\
\hline
\end{tabular}
\end{center}
\caption{The parse table for the grammar in \figref{fig:ll-grammar}}
\label{tab:parse-table}
\end{table}

\section{Using a parse table}
\label{sec:use-parse-table}

Parsing begins with the start symbol $\StartSymb$ followed by $\eof$
in the stack and the entire input.
It proceeds by matching the leftmost symbol in the stack against the
leftmost symbol $t$ in the input.
If the leftmost symbol in the stack is a terminal and it matches the
leftmost symbol in the input, the terminal is consumed; in other
words, it is removed from both the stack and the input.
In practice, consuming an input token often means storing in an object
that is being parsed.
If the leftmost symbol in the stack is a nonterminal $N$, the action
specified in the parse table for row $N$ and column $t$ is performed,
reducing $N$.
If there is no action, the input is not in the language and the input
string is rejected.
\tabref{tab:parse} demonstrates this process on the input \syn{(1 + 2)
* 3}.

\begin{table}
\begin{center}
\begin{tabular}{r | r | l}
Stack & Input & Action \\
\hline
$E\eof$ & \syn{(1 + 2) * 3}\eof & $E \produces T E'$ \\
$T E'\eof$ & \syn{(1 + 2) * 3}\eof & $T \produces F T'$ \\
$F T' E'\eof$ & \syn{(1 + 2) * 3}\eof & $F \produces \syn{(} E \syn{)}$ \\
$\syn{(} E \syn{)} T' E'\eof$ & \syn{(1 + 2) * 3}\eof & consume \\
$E \syn{)} T' E'\eof$ & \syn{1 + 2) * 3}\eof & $E \produces T E'$ \\
$T E' \syn{)} T' E'\eof$ & \syn{1 + 2) * 3}\eof & $T \produces F T'$ \\
$F T' E' \syn{)} T' E'\eof$ & \syn{1 + 2) * 3}\eof & $F \produces
\syn{int}$ \\
$\syn{int}\ T' E' \syn{)} T' E'\eof$ & \syn{1 + 2) * 3}\eof & consume \\
$T' E' \syn{)} T' E'\eof$ & \syn{+ 2) * 3}\eof & $T' \produces
\emptystring$ \\
$E' \syn{)} T' E'\eof$ & \syn{+ 2) * 3}\eof & $E' \produces + T E'$ \\
$+ T E' \syn{)} T' E'\eof$ & \syn{+ 2) * 3}\eof & consume \\
$T E' \syn{)} T' E'\eof$ & \syn{2) * 3}\eof & $T \produces F T'$ \\
$F T' E' \syn{)} T' E'\eof$ & \syn{2) * 3}\eof & $F \produces \syn{int}$ \\
$\syn{int}\ T' E' \syn{)} T' E'\eof$ & \syn{2) * 3}\eof & consume \\
$T' E' \syn{)} T' E'\eof$ & \syn{) * 3}\eof & $T' \produces \emptystring$ \\
$E' \syn{)} T' E'\eof$ & \syn{) * 3}\eof & $E' \produces \emptystring$ \\
$\syn{)} T' E'\eof$ & \syn{) * 3}\eof & consume \\
$T' E'\eof$ & \syn{* 3}\eof & $T' \produces * F T'$ \\
$* F T' E'\eof$ & \syn{* 3}\eof & consume \\
$F T' E'\eof$ & \syn{3}\eof & $F \produces \syn{int}$ \\
$\syn{int}\ T' E'\eof$ & \syn{3}\eof & consume \\
$T' E'\eof$ & \eof & $T' \produces \emptystring$ \\
$E'\eof$ & \eof & $T' \produces \emptystring$ \\
$\eof$ & \eof & accept \\
\end{tabular}
\end{center}
\caption{Parsing \syn{(1 + 2) * 3} using \tabref{tab:parse-table}}
\label{tab:parse}
\end{table}

% \bibliographystyle{plain}
% \bibliography{zipper}
\end{document}
