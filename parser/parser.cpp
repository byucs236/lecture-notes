#include <queue>
#include <assert.h>
#include <iostream>
#include <string>

using namespace std;

enum token {ART, ADJ, NOUN, VERB, ADV, MEOF};

string toString(token t) {
	switch(t) {
	case ART: return ("ART");
	case ADJ: return ("ADJ");
	case NOUN: return ("NOUN");
	case VERB: return ("VERB");
	case ADV: return ("ADV");
	case MEOF: return ("MEOF");
	default: break;
	}
	return ("Uh oh!");
}

void initializeTokens(queue<token>& tokens) {
	tokens.push(ART);
	tokens.push(NOUN);
	tokens.push(VERB);
	tokens.push(ADV);
	tokens.push(ADV);
	tokens.push(MEOF);
}

bool check(queue<token>& tokens, token expected) {
	token given = tokens.front();
	tokens.pop();
	if (given == expected) {
		cout << toString(given) << " ";
		return true;
	}
	cout << endl << "ERROR: expected " << toString(expected) << " but found " << toString(given);
	return false;
}

bool checkART(queue<token>& tokens) {
	return check(tokens, ART);
}

bool checkADJ(queue<token>& tokens) {
	return check(tokens, ADJ);
}

bool checkNOUN(queue<token>& tokens) {
	return check(tokens, NOUN);
}

bool checkVERB(queue<token>& tokens) {
	return check(tokens, VERB);
}

bool checkADV(queue<token>& tokens) {
	return check(tokens, ADV);
}

bool checkMEOF(queue<token>& tokens) {
	return check(tokens, MEOF);
}

bool x(queue<token>& tokens) {
	token t = tokens.front();

	if (t == ADJ) {
		return (checkADJ(tokens) && checkNOUN(tokens));
	}
	
	if (t == NOUN) {
		return checkNOUN(tokens);
	}

	return false;
}

bool np(queue<token>& tokens) {
	if (tokens.front() != ART) {
		return false;
	}
	return (checkART(tokens) && x(tokens));
}

bool y(queue<token>& tokens) {
	token t = tokens.front();

	if (t == ADV) {
		return checkADV(tokens);
	}

	if (t == MEOF) {
		return true;
	}

	return false;
}

bool vp(queue<token>& tokens) {
	if (tokens.front() != VERB) {
		return false;
	}
	return (checkVERB(tokens) && y(tokens));
}

bool s(queue<token>& tokens) {
	if (tokens.front() != ART) {
		return false;
	}

	return (np(tokens) && vp(tokens) && checkMEOF(tokens));
}

int main(int argc, char* argv[]) {
	queue<token> tokens;
	initializeTokens(tokens);
	if (!s(tokens)) {
		cout << endl << "Failure!" << endl;
	}
	else {
		cout << endl << "Success!" << endl;
	}
	return 0;
}
