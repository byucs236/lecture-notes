#include <queue>
#include <assert.h>
#include <iostream>
#include <string>

using namespace std;

enum token {PLUS, ASTERISK, LEFT_PAREN, RIGHT_PAREN, ID, MEOF};

void e(queue<token>& tokens);

string to_string(token t) {
	switch(t) {
	case PLUS: return ("+");
	case ASTERISK: return ("*");
	case LEFT_PAREN: return ("(");
	case RIGHT_PAREN: return (")");
	case ID: return ("ID");
	case MEOF: return ("MEOF");
	default: break;
	}
	return ("Uh oh!");
}

void initialize_tokens(queue<token>& tokens) {
	tokens.push(LEFT_PAREN);
	tokens.push(ID);
	tokens.push(PLUS);
	tokens.push(ID);
	tokens.push(RIGHT_PAREN);
	tokens.push(ASTERISK);
	tokens.push(ID);
	tokens.push(MEOF);
}

void check(queue<token>& tokens, token expected) {
	token given = tokens.front();
	tokens.pop();
	
	if (given != expected) {
		throw(to_string(given));
	}

	cout << to_string(given) << " ";
}


void f(queue<token>& tokens) {
	token t = tokens.front();

	if (t == LEFT_PAREN) {
		check(tokens, LEFT_PAREN);
		e(tokens);
		check(tokens, RIGHT_PAREN);
		return;
	}

	if (t == ID) {
		check(tokens, ID);
		return;
	}
	
	throw(to_string(t));
}

void t_prime(queue<token>& tokens) {
	token t = tokens.front();
	
	if (t == PLUS || t == RIGHT_PAREN || t == MEOF) {
		return;
	}

	if (t == ASTERISK) {
		check(tokens, ASTERISK);
		f(tokens);
		t_prime(tokens);
		return;
	}

	throw(to_string(t));
}

void t(queue<token>& tokens) {
	f(tokens);
	t_prime(tokens);
}

void e_prime(queue<token>& tokens) {
	token t = tokens.front();
	
	if (t == RIGHT_PAREN || t == MEOF) {
		return;
	}

	if (t == PLUS) {
		check(tokens, PLUS);
		::t(tokens);
		e_prime(tokens);
		return;
	}

	throw(to_string(t));
}

void e(queue<token>& tokens) {
	t(tokens);
	e_prime(tokens);
}



int main(int argc, char* argv[]) {
	queue<token> tokens;
	initialize_tokens(tokens);
	try {
		e(tokens);
		cout << endl;
	}
	catch (std::string s) {
		cerr << "Failure!" << endl << "  " << s << endl;
	}
}
